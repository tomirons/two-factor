<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['middleware' => ['auth', 'authy.registered']], function () {
    Route::get('/', function () {
        return view('welcome');
    });
});

Route::get('chat', 'ChatController@index');

Route::get('chat/messages', 'ChatController@allMessages');

Route::post('chat/message', 'ChatController@message');

Route::get('two-factor', 'AuthyController@getTwoFactor');

Route::post('two-factor', 'AuthyController@postTwoFactor');

Route::get('two-factor/verify', 'AuthyController@getVerify');

Route::post('two-factor/verify', 'AuthyController@postVerify');

Auth::routes();
