@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                @include('flash::message')
                <div class="panel panel-default">
                    <div class="panel-heading">Verification</div>

                    <div class="panel-body">
                        {!! Form::open( [ 'url' => 'two-factor/verify' ] ) !!}
                            <div class="form-group">
                                {!! Form::label( 'token', 'Token' ) !!}
                                {!! Form::number( 'token', null, [ 'class' => 'form-control' ] ) !!}
                            </div>
                            {!! Form::submit( 'Submit', [ 'class' => 'btn btn-primary' ] ) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
