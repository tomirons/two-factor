@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Two Factor Authentication</div>

                    <div class="panel-body">
                        {!! Form::model( auth()->user(), [ 'url' => 'two-factor' ] ) !!}
                            <div class="form-group">
                                {!! Form::label( 'phone_number', 'Phone Number' ) !!}
                                {!! Form::text( 'phone_number', null, [ 'class' => 'form-control' ] ) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label( 'phone_country_id', 'Country Code' ) !!}
                                {!! Form::select( 'phone_country_id', Countries::select( DB::raw( 'concat(name, " (+", calling_code, ")") as name, id' ) )->orderBy( 'name' )->pluck( 'name', 'id' ), null, [ 'placeholder' => 'Pick a country...', 'class' => 'form-control' ] ) !!}
                            </div>
                            {!! Form::submit( 'Submit', [ 'class' => 'btn btn-primary' ] ) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
