<?php

namespace App\Http\Controllers;

use App\Events\UserHasSentAMessage;
use App\Message;
use Illuminate\Http\Request;

use App\Http\Requests;

class ChatController extends Controller
{
    public function index()
    {
        return view('layouts.chat.index');
    }

    public function message(Request $request)
    {
        $message = $request->user()->messages()->create([
            'content' => $request->message
        ]);

        event(new UserHasSentAMessage($message));

        return $message;
    }

    public function allMessages()
    {
        return Message::with('user')->get();
    }
}
