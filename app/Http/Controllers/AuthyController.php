<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Srmklive\Authy\Facades\Authy;

class AuthyController extends Controller
{
    public function __construct()
    {
        $this->middleware( 'auth' );
    }
    public function getTwoFactor()
    {
        return view( 'layouts.twofactor' );
    }

    public function postTwoFactor( Request $request )
    {
        $user = $request->user();

        $user->fill( $request->all() );

        $user->phone_country_code = $user->country->calling_code;

        /* Register the user with authy */
        Authy::getProvider()->register( $user );

        $user->save();

        /* Send token to the user */
        Authy::getProvider()->sendSmsToken( $user );

        flash()->success( 'We\'ve sent a code to your phone, please enter it here.' );

        return redirect( 'two-factor/verify' );
    }

    public function getVerify()
    {
        return view( 'layouts.verify' );
    }

    public function postVerify( Request $request )
    {
        $user = $request->user();

        /* Verify the code */
        if ( Authy::getProvider()->tokenIsValid( $user, $request->token ) )
        {
            $user->authy_updated_at = Carbon::now();
            $user->save();

            cookie( 'authy_', null, 10 );

            flash()->success( 'Your account has been successfully activated.' );

            return redirect( '/' );
        }
        else
        {
            Authy::getProvider()->sendSmsToken( $user );

            flash()->error( 'Something went wrong, we\'ve sent you another code.' );

            return redirect()->back();
        }
    }
}
