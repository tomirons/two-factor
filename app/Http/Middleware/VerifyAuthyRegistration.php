<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;

class VerifyAuthyRegistration
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /* Check if the user is logged in and is within the 30 day period */
        if ($request->user() && ($request->user()->authy_updated_at === null || $request->user()->authy_updated_at->diffInDays(Carbon::now()) >= 30)) {
            return redirect('two-factor');
        }

        return $next($request);
    }
}
